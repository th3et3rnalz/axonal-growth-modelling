import matplotlib.pyplot as plt
from scipy.integrate import ode
import numpy as np

lambda_0 = 1.5
alpha_c = 1.1
k = 1
tau = 1 / (k*alpha_c)
tau_p = 5 * tau

t = np.linspace(start=0, stop=20, num=200)

# ======== Stretch and hold experiment ========
gamma = np.exp(-t)*(1 - lambda_0/alpha_c) + lambda_0/alpha_c
alpha = lambda_0 / gamma

plt.title("Stretch and hold experiment")
plt.plot(t, gamma, label=r"$\gamma$")
plt.plot(t, alpha, label=r"$\alpha$")
plt.plot(t, [lambda_0]*np.ones_like(t), label=r"$\lambda$")
plt.legend()
plt.show()


# ======== Speed controlled traction experiment ========
t = np.logspace(start=-2, stop=2, num=200)
lambda_v = 1 + t / tau_p
gamma = t / (alpha_c*tau_p) + (1-tau/tau_p)/alpha_c + (1 - (1-tau/tau_p)/alpha_c)*np.exp(-t/tau)
alpha = lambda_v/gamma

plt.title("Speed controlled traction experiment")
plt.loglog(t, gamma-1, label=r"$\gamma - 1$")
plt.loglog(t, alpha-1, label=r"$\alpha - 1$")
plt.loglog(t, lambda_v-1, label=r"$\lambda - 1$")
plt.legend()
plt.show()

# ======== Alternative Speed controlled traction experiment ========
t0 = 0
g0 = 1
t_lst = []
g_lst = []


def f(t, g):
    return max(0, k*((1+t/tau_p)/g - alpha_c))


integrator = ode(f=f).set_integrator('zvode', method='bdf')
integrator.set_initial_value(g0, t0)

t1 = 20
dt = 0.1

while integrator.successful() and integrator.t < t1:
    t_lst.append(integrator.t+dt)
    g_lst.append(integrator.integrate(integrator.t+dt))

g_lst = np.real(np.array(g_lst)).reshape(len(g_lst))
t_lst = np.array(t_lst)

lambda_lst = 1 + t_lst/tau_p
alpha_lst = lambda_lst / g_lst

plt.title("Time delayed speed controlled traction")
plt.loglog(t_lst, g_lst - 1, label=r"$\gamma - 1$")
plt.loglog(t_lst, lambda_lst - 1, label=r"$\lambda - 1$")
plt.loglog(t_lst, alpha_lst - 1, label=r"$\alpha - 1$")
plt.legend()
plt.ylim((2e-2, 8))
plt.show()
