from model_2 import plot_numerical
from model_1 import plot_exact
from helpers import Config
import matplotlib.pyplot as plt


plt.subplot(2, 3, 1)
c = Config()
plt.title("A vs B")
plot_numerical(n_points=1000, t=1, label=r"$D \neq 0$", c=c)
plot_exact(t=1, label="$D = 0$", c=c)
plt.legend()

# let's vary the force applied
plt.subplot(2, 3, 2)
plt.title("A - Varying force (t=1)")
c = Config()
plot_exact(t=1, label="$F_0=1$", c=c)
c.f_0 *= 2  # * Config().f_0
plot_exact(t=1, label="$F_0=2$", c=c)
c.f_0 *= 0.1/2 * Config().f_0
plot_exact(t=1, label="$F_0=0.1$", c=c)
plt.legend()


plt.subplot(2, 3, 3)
c = Config()
plt.title("B - Varying force (t=1)")
plot_numerical(n_points=500, t=1, label="$F_0*=1$", c=c)
c.f_0 = 2 * Config().f_0
plot_numerical(n_points=500, t=1, label="$F_0*=2$", c=c)
c.f_0 = 0.1 * Config().f_0
plot_numerical(n_points=500, t=1, label="$F_0*=0.1$", c=c)
plt.legend()


plt.subplot(2, 3, 4)
c = Config()
plt.title("B - Varying force (t=0)")
plot_numerical(n_points=500, t=0, label="$F_0*=1$", c=c)
c.f_0 = 2 * Config().f_0
plot_numerical(n_points=500, t=0, label="$F_0*=2$", c=c)
c.f_0 = 0.1 * Config().f_0
plot_numerical(n_points=500, t=0, label="$F_0*=0.1$", c=c)
plt.legend()


plt.subplot(2, 3, 5)
c = Config()
for t_i in [1]:
    for a_c in [0.1, 1, 10]:
        c_i = Config()
        c_i.a_c *= a_c

        plot_numerical(n_points=500, t=t_i, label=f"$a_c*=${a_c}", c=c_i)

    plt.title("B - Changing $a_c$ at t=1")
    plt.legend()


plt.subplot(2, 3, 6)
for t_i in [1]:
    for eta in [0.1, 1, 10]:
        c_i = Config()
        c_i.D *= eta

        plot_numerical(n_points=500, t=t_i, label=rf"$D*=${eta}", c=c_i)

    plt.title(r"B - Changing $D$ at t=1")
    plt.legend()


plt.tight_layout()
plt.show()
