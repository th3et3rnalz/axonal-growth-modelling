from helpers import dv_dx, v, l, Config
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import root


def create_a_matrix(n_points, c_0, c, t=0):
    x_start = 0
    x_end = l(t=t, c=c)
    delta_x = (x_end - x_start) / n_points
    a = np.zeros(shape=(n_points, n_points))
    b = np.zeros(shape=(n_points, 1))
    for row in range(1, n_points-1):
        x_i = x_start + delta_x*row
        a[row, row-1] = -c.D
        a[row, row] = 2*c.D - (c.a_c + v(x=x_i, t=t, c=c))*delta_x + delta_x*delta_x*dv_dx(x=x_i, t=t, c=c)
        a[row, row+1] = delta_x*(c.a_c + v(x=x_i, t=t, c=c)) - c.D

        b[row] = - c.rho*dv_dx(x=x_i, t=t, c=c)*delta_x*delta_x

    a[0, 0] = 1
    b[0] = c_0

    # Setting the no flux BC at x=l
    a[n_points-1, n_points-1] = 1 - delta_x*(c.a_c + v(x=x_end, t=t, c=c))/c.D
    a[n_points-1, n_points-2] = -1
    b[n_points-1] = 0

    x = np.linspace(start=x_start, stop=x_end, num=n_points)

    return a, b, x


def plot_numerical(n_points, t, label, c):
    def solve_it(c_0, t, n_points):
        a, b, x = create_a_matrix(n_points=n_points, c_0=c_0[0], t=t, c=c)
        c_numerical = np.linalg.solve(a=a, b=b)
        return c_numerical[-1]

    result = root(solve_it, np.array([0.239]), args=(t, n_points))

    a, b, x = create_a_matrix(n_points=n_points, c_0=result['x'][0], t=t, c=c)
    c_numerical = np.linalg.solve(a=a, b=b)

    plt.plot(x, c_numerical, label=label)


if __name__ == "__main__":
    c_1 = Config()

    plot_numerical(n_points=1000, t=1, label="Numerical (n_points=100)", c=c_1)
    plt.grid()
    plt.title("Solution to Model 2")
    plt.ylabel("Free Tubulin concentration")
    plt.xlabel("x distance from the axon")
    plt.legend()
    plt.tight_layout()
    plt.show()
