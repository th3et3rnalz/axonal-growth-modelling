import matplotlib.pyplot as plt
import numpy as np
from helpers import dv_dx, v, l, Config


def h_1(x, t, c):
    return c.a_c + v(x=x, t=t, c=c)


def h_2(x, t, c):
    return dv_dx(x=x, t=t, c=c)


def h_3(x, t, c):
    return -c.rho*dv_dx(x=x, t=t, c=c)


def create_a_matrix(n_points, x_start, t=0, c=Config):
    x_end = l(t=t, c=c)
    delta_x = (x_end - x_start) / n_points
    a = np.zeros(shape=(n_points-1, n_points-1))
    b = np.zeros(shape=(n_points-1, 1))
    for row in range(n_points-2):
        x_i = x_start + delta_x*row
        a[row, row] = h_2(x=x_i, t=t, c=c)*delta_x - h_1(x=x_i, t=t, c=c)
        a[row, row+1] = h_1(x=x_i, t=t, c=c)

        b[row] = delta_x*h_3(x=x_i, t=t, c=c)

    # Just putting in the last point.
    x_i = x_start + (n_points-1)*delta_x
    a[n_points-2, n_points-2] = h_2(x=x_i, t=t, c=c)*delta_x - h_1(x=x_i, t=t, c=c)
    b[n_points-2] = delta_x*h_3(x=x_i, t=t, c=c)

    # Returning this so everyone is on the same page about the grid used.
    x = np.linspace(start=x_start, stop=x_end, num=n_points)

    return a, b, x


def plot_numerical(n_points, t, label, c=Config):
    a, b, x = create_a_matrix(n_points=n_points, x_start=0, t=t, c=c)
    c_numerical = np.linalg.solve(a=a, b=b)
    c_numerical = np.append(c_numerical, 0)
    plt.plot(x, c_numerical, label=label)


t_main = 0
plot_numerical(n_points=5, t=t_main, label=f"Numerical (n_points=5)")
plot_numerical(n_points=50, t=t_main, label=f"Numerical (n_points=50)")


def plot_exact(t, label, c):
    x = np.linspace(0, l(t, c=c), 100)
    c_exact = [c.rho*(v(x=l(t, c=c), t=t, c=c) - v(x=x_i, t=t, c=c)) / (c.a_c + v(x=x_i, t=t, c=c)) for x_i in x]
    plt.plot(x, c_exact, label=label)


if __name__ == "__main__":
    plot_exact(t=t_main, label="Exact", c=Config())
    plt.grid()
    plt.title(f"Solution to Model 1 at t={t_main}")
    plt.ylabel("Free Tubulin concentration")
    plt.xlabel("x distance from the axon")
    plt.legend()
    plt.tight_layout()
    plt.show()

    plt.subplot(2, 2, 1)
    for t_i in [1]:
        for f in [0.1, 1, 2]:
            c_i = Config()
            c_i.f_0 = f
            # c_i.g = 3
            plot_exact(t=t_i, label=f"f={f:.1f}", c=c_i)

        plt.title(f"Changing $F_0$ at t={t_i}")
        plt.xlabel("Distance from the axon")
        plt.ylabel("Free tubulin concentration")

        plt.legend()
        # plt.show()

    plt.subplot(2, 2, 2)
    for t_i in [1]:
        for a_c in [0.1, 1, 10]:
            c_i = Config()
            c_i.a_c = a_c

            plot_exact(t=t_i, label=f"$a_c=${a_c}", c=c_i)

        plt.title("Changing $a_c$ at t=1")
        plt.legend()

    plt.subplot(2, 2, 3)
    for t_i in [1]:
        for rho in [0.1, 1, 10]:
            c_i = Config()
            c_i.rho = rho

            plot_exact(t=t_i, label=rf"$\rho=${rho}", c=c_i)

        plt.title(r"Changing $\rho$ at t=1")
        plt.legend()

    plt.subplot(2, 2, 4)
    for t_i in [1]:
        for eta in [0.1, 1, 10]:
            c_i = Config()
            c_i.eta = eta

            plot_exact(t=t_i, label=rf"$\eta=${eta}", c=c_i)

        plt.title(r"Changing $\eta$ at t=1")
        plt.legend()

    plt.tight_layout()
    plt.show()
