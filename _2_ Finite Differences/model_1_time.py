import matplotlib.pyplot as plt
from helpers import v, l, Config
import numpy as np


c = Config()


def compute_concentration_and_domain(t: float, c):
    x = np.linspace(0, l(t=t, c=c), 100)
    conc = c.rho * (v(x=l(t=t, c=c), t=t, c=c) - v(x=x, t=t, c=c)) / (c.a_c + v(x=x, t=t, c=c))
    return x, conc


c_0_lst = []
t_lst = []
plt.subplot(2, 1, 1)

for t in np.linspace(0, 5, 100 + 1):
    x, conc = compute_concentration_and_domain(t=t, c=c)
    if t % 1 == 0.0:
        plt.plot(x, conc, label=f"t={t}")

    c_0_lst.append(conc[0])
    t_lst.append(t)

plt.title("Concentration distribution for different times")
plt.ylabel("c(x; t)")
plt.xlabel("x - Distance from soma")
plt.legend()

plt.subplot(2, 1, 2)
plt.title("Concentration at the soma")
plt.plot(t_lst, c_0_lst)
plt.ylabel("c_0(t)")
plt.xlabel("Time")

plt.tight_layout()
plt.show()
