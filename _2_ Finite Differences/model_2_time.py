from model_2 import create_a_matrix
from helpers import Config
from scipy.optimize import root
import matplotlib.pyplot as plt
import numpy as np

l_0 = 1
c_0_lst = []
t_lst = []
n_points = 1000
plt.subplot(2, 1, 1)

for t in np.linspace(0, 5, 10 + 1):
    c = Config()

    def solve_it(c_0, t=t):
        a, b, x = create_a_matrix(n_points=n_points, c_0=c_0[0], t=t, c=c)
        c_numerical = np.linalg.solve(a=a, b=b)
        return c_numerical[-1]

    if len(c_0_lst) == 0:
        result = root(fun=solve_it, x0=np.array([0.239]))
    else:
        result = root(fun=solve_it, x0=np.array([c_0_lst[-1]]))

    a, b, x = create_a_matrix(n_points=n_points, c_0=result['x'][0], t=t, c=c)
    c = np.linalg.solve(a=a, b=b)

    if t % 1 == 0.0:
        plt.plot(x, c, label=f"t={t}")

    c_0_lst.append(c[0])
    t_lst.append(t)


plt.title("Concentration distribution for different times")
plt.ylabel("c(x; t)")
plt.xlabel("x - Distance from soma")
plt.legend()

plt.subplot(2, 1, 2)
plt.title("Concentration at the soma")
plt.plot(t_lst, c_0_lst)
plt.ylabel("c_0(t)")
plt.xlabel("Time")

plt.tight_layout()
plt.show()


