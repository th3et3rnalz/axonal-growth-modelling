"""
UPDATE:

There's no need to delete the file, but we will not be using this model for the final report.
The problem of tubulin degradation has been sufficiently studied in the previous work.
"""

import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import root

# f_0 = 2e-9
# g = 3.9e13
# eta = 9.6e3
# l_0 = 2e-4
# a_c = 1e-8
# rho = 1e-5
# D = 1e-12

f_0 = 1
g = 1
eta = 1
l_0 = 1
a_c = 1
rho = 1
D = 1
kappa = 1e-8


def l(t):
    alpha = np.sqrt(eta/g)
    return np.arcsinh(np.sinh(alpha*l_0) * np.exp(f_0*t/g)) / alpha


def v(x, t):
    alpha = np.sqrt(eta/g)
    return f_0*np.sinh(alpha*x) / (np.sqrt(g*eta)*np.cos(alpha*l(t)))


def dv_dx(x, t):
    alpha = np.sqrt(eta / g)
    return f_0*np.cosh(alpha*x) / (g*np.cosh(alpha*l(t)))


def create_a_matrix(n_points, x_start, x_end, c_0, t=0):
    delta_x = (x_end - x_start) / n_points
    a = np.zeros(shape=(n_points, n_points))
    b = np.zeros(shape=(n_points, 1))
    for row in range(1, n_points-1):
        x_i = x_start + delta_x*row
        a[row, row-1] = -D
        a[row, row] = 2*D - (a_c + v(x=x_i, t=t))*delta_x + delta_x*delta_x*dv_dx(x=x_i, t=t) + kappa
        a[row, row+1] = delta_x*(a_c + v(x=x_i, t=t)) - D

        b[row] = - rho*dv_dx(x=x_i, t=t)*delta_x*delta_x

    # Setting the Robin boundary condition at x=0
    a[0, 0] = 1
    b[0] = c_0

    # Setting the no flux BC at x=l
    a[n_points-1, n_points-1] = 1 - delta_x*(a_c + v(x=x_end, t=t))/D
    a[n_points-1, n_points-2] = -1
    b[n_points-1] = 0

    x = np.linspace(start=x_start, stop=x_end, num=n_points)

    return a, b, x


def solve_it(c_0):
    a, b, x = create_a_matrix(5000, 0, l_0, c_0=c_0[0])
    c_numerical = np.linalg.solve(a=a, b=b)
    return c_numerical[-1]


result = root(solve_it, np.array([0.2422]))

print(result)

a, b, x = create_a_matrix(5000, 0, l_0, c_0=result['x'][0])
c_numerical = np.linalg.solve(a=a, b=b)
plt.plot(x, c_numerical, label="Numerical (n_points=10000)")
plt.grid()
plt.title("Solution to Model 3")
plt.ylabel("Free Tubulin concentration")
plt.xlabel("x distance from the axon")
plt.legend()
plt.tight_layout()
plt.show()
