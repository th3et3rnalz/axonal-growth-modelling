import numpy as np


class Config:
    f_0 = 1
    g = 1
    eta = 1
    l_0 = 1
    a_c = 1
    rho = 1
    D = 1


# class Config:
#     f_0 = 2e-9
#     g = 3.9e13
#     eta = 9.6e3
#     l_0 = 2e-4
#     a_c = 1e-8
#     D = 1e-12
#     rho = 1


def l(t, c):
    alpha = np.sqrt(c.eta/c.g)
    return np.arcsinh(np.sinh(alpha*c.l_0) * np.exp(c.f_0*t/c.g)) / alpha


def v(x, t, c):
    alpha = np.sqrt(c.eta/c.g)
    return c.f_0*np.sinh(alpha*x) / (np.sqrt(c.g*c.eta)*np.cosh(alpha*l(t=t, c=c)))


def dv_dx(x, t, c):
    alpha = np.sqrt(c.eta / c.g)
    return c.f_0*np.cosh(alpha*x) / (c.g*np.cosh(alpha*l(t=t, c=c)))
