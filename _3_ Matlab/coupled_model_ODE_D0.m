% AUTHOR: Boyang Han

% parameters of the model

t = 1;  % time of the cross-section
G = 3.9e13;
eta = 9.6e3;
l0 = 2e-4;
beta = sinh(l0*sqrt(eta/G));
F0 = 2e-9;
D = 1e-12;
a = 1e-8;
rho = 1;


% ODE

l = func_l(t, G, eta, beta, F0);
v_right = func_v(l, l, G, eta, F0);
dcdx_left = @(x, y) [-((rho + y(1))*func_dvdx(x, l, G, eta, F0))/(a + func_v(x, l, G, eta, F0))];
dcdx_right = @(x, y) [((rho + y(1))*func_dvdx(l-x, l, G, eta, F0))/(a + func_v(l-x, l, G, eta, F0))];
x0_left = func_c0(t);
x0_right = 0;
xspan = [0, l];
sol_left = ode45(dcdx_left, xspan, [x0_left]);
sol_right = ode45(dcdx_right, xspan, [x0_right]);

x_grid_left = linspace(0, l, 100);
x_grid_right = linspace(0, l, 100);
y_left = deval(sol_left, x_grid_left);
y_right = deval(sol_right, x_grid_right);
plot(x_grid_left, y_left, flip(x_grid_right), y_right)
figure()
plot(x_grid_left, y_left)
figure()
plot(flip(x_grid_right), y_right)

% functions used

function l = func_l(t, G, eta, beta, F0)
    l = sqrt(G/eta)*asinh(beta*exp(F0*t/G));
end

function v = func_v(x, l, G, eta, F0)
    v = (F0*sinh(x*sqrt(eta/G)))/(sqrt(eta*G)*cosh(l*sqrt(eta/G)));
end

function dvdx = func_dvdx(x, l, G, eta, F0)
    dvdx = (F0*cosh(x*sqrt(eta/G)))/(G*cosh(l*sqrt(eta/G)));
end

function c0 = func_c0(t)  % left boundary
    c0 = 2*t;
end
