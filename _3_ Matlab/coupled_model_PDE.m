% AUTHOR: Boyang Han

% parameters of the model

% G = 1;
% eta = 1;
% l0 = 1;
% F0 = 1;
% D = 1;
% a = 1;
% rho = 1;
G = 3.9e13;
eta = 9.6e3;
l0 = 2e-4;
beta = sinh(l0*sqrt(eta/G));
F0 = 2e-9;
D = 1e-12;
a = 1e-8;
rho = 1;


% parameters of FD scheme

J = 10;  % number of spatial grid points
dy = 1/J;  % assume the range of y to be [0, 1]
T = 0.1;  % time horizon
dt = 0.001;  % time step
N = T/dt;  % number of time steps

y_grid = linspace(0, 1, J+1)';  % spatial grid
t_grid = linspace(0, T, N+1)';  % time grid
C_values = zeros(N+1, J+1);  % concentration

% initial and left boundary values

C_values(1, :) = func_c_initial(l0*y_grid');
C_values(2:end, 1) = func_c0(t_grid(2:end));

% compute the others

for n=0:N-1
    [A_left, A_right, F] = FD(J, n, dy, dt, G, eta, beta, F0, a, D, rho);
    C_values(n+2, 2:end) = A_left\(A_right*(C_values(n+1, 2:end)') + F);
end

[yy, tt] = meshgrid(y_grid, t_grid);
surf(tt, yy, C_values);
xlabel('t');
ylabel('y');
zlabel('c');

% functions used

function l = func_l(t, G, eta, beta, F0)
    l = sqrt(G/eta)*asinh(beta*exp(F0*t/G));
end

function dldt = func_dldt(t, G, eta, beta, F0)
    dldt = sqrt(G/eta)*(beta*F0*exp(F0*t/G))/(G*sqrt((beta^2)*exp(2*F0*t/G) + 1));
end

function v = func_v(y, l, G, eta, F0)
    v = (F0*sinh(y*l*sqrt(eta/G)))/(sqrt(eta*G)*cosh(l*sqrt(eta/G)));
end

function dvdx = func_dvdx(y, l, G, eta, F0)
    dvdx = (F0*cosh(y*l*sqrt(eta/G)))/(G*cosh(l*sqrt(eta/G)));
end

function c0 = func_c0(t)  % left boundary
    c0 = 2*t;
end

function c_initial = func_c_initial(y)  % initial condition
    c_initial = y;
end

function coeff_dcdy = func_coeff_dcdy(y, l, dldt, v, a)  % coefficient of dc/dy term
    coeff_dcdy = -(a + v - y*dldt)/l;
end

function coeff_d2cdy2 = func_coeff_d2cdy2(l, D)  % coefficient of d^2 c/d y^2 term
    coeff_d2cdy2 = D/(l^2);
end

function [A_left, A_right, F] = FD(J, n, dy, dt, G, eta, beta, F0, a, D, rho)
    y = linspace(1/J, 1, J)';
    t_old = n*dt;
    t_new = t_old + dt;
    l_old = func_l(t_old, G, eta, beta, F0);
    l_new = func_l(t_new, G, eta, beta, F0);
    dldt_old = func_dldt(t_old, G, eta, beta, F0);
    dldt_new = func_dldt(t_new, G, eta, beta, F0);
    v_old = func_v(y, l_old, G, eta, F0);
    v_new = func_v(y, l_new, G, eta, F0);
    dvdx_old = func_dvdx(y, l_old, G, eta, F0);
    dvdx_new = func_dvdx(y, l_new, G, eta, F0);
    coeff_dcdy_old = func_coeff_dcdy(y, l_old, dldt_old, v_old, a);
    coeff_dcdy_new = func_coeff_dcdy(y, l_new, dldt_new, v_new, a);
    coeff_d2cdy2_old = func_coeff_d2cdy2(l_old, D);
    coeff_d2cdy2_new = func_coeff_d2cdy2(l_new, D);

    A_left_diag = 1 + (dt*coeff_d2cdy2_new)/(dy^2) + dt*dvdx_new/2;
    A_left_subdiag = (dt*coeff_dcdy_new)/(4*dy) - (dt*coeff_d2cdy2_new)/(2*dy^2);
    A_left_superdiag = -(dt*coeff_dcdy_new)/(4*dy) - (dt*coeff_d2cdy2_new)/(2*dy^2);
    A_right_diag = 1 - (dt*coeff_d2cdy2_old)/(dy^2) - dt*dvdx_old/2;
    A_right_subdiag = -(dt*coeff_dcdy_old)/(4*dy) + (dt*coeff_d2cdy2_old)/(2*dy^2);
    A_right_superdiag = (dt*coeff_dcdy_old)/(4*dy) + (dt*coeff_d2cdy2_old)/(2*dy^2);
    A_left = diag(A_left_diag) + diag(A_left_subdiag(2:end), -1) + diag(A_left_superdiag(1:end-1), 1);
    A_left(J, J-1) = D/(D - l_new*dy*(a + v_new(end)));
    A_left(J, J) = 1;
    A_right = diag(A_right_diag) + diag(A_right_subdiag(2:end), -1) + diag(A_right_superdiag(1:end-1), 1);
    A_right(J, :) = zeros(1, J);
    F = -rho*dt*(dvdx_new + dvdx_old)/2;
    F(1, 1) = F(1, 1) + A_right_subdiag(1, 1)*func_c0(t_old) - A_left_subdiag(1, 1)*func_c0(t_new);
    F(J, 1) = 0;
end
    

    
