% AUTHOR: Boyang Han

% parameters of the model

t = 1;  % time of the cross-section
G = 3.9e13;
eta = 9.6e3;
l0 = 2e-4;
beta = sinh(l0*sqrt(eta/G));
F0 = 2e-9;
D = 1e-12;
a = 1e-8;
rho = 1;


% ODE

l = func_l(t, G, eta, beta, F0);
v_right = func_v(l, l, G, eta, F0);
dc = @(x, y) [y(2); ((a + func_v(y(1), l, G, eta, F0))*y(2))/D + func_dvdx(y(1), l, G, eta, F0)*(y(1)/D + rho)];
BCs = @(ca, cb) [ca(1) - func_c0(t); cb(2) - ((a + v_right)*cb(1))/D];
solinit = bvpinit(linspace(0, l, 100), [1, 0]);
sol = bvp5c(dc, BCs, solinit);

x_grid = linspace(0, l, 100);
y = deval(sol, x_grid);
plot(x_grid, y(1, :))

% functions used

function l = func_l(t, G, eta, beta, F0)
    l = sqrt(G/eta)*asinh(beta*exp(F0*t/G));
end

function v = func_v(x, l, G, eta, F0)
    v = (F0*sinh(x*sqrt(eta/G)))/(sqrt(eta*G)*cosh(l*sqrt(eta/G)));
end

function dvdx = func_dvdx(x, l, G, eta, F0)
    dvdx = (F0*cosh(x*sqrt(eta/G)))/(G*cosh(l*sqrt(eta/G)));
end

function c0 = func_c0(t)  % left boundary
    c0 = 2*t;
end
